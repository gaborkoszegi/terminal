/**
 * Created by krisztian.mariassy on 19/05/2017.
 */

var rssfeedreader = require(__dirname + '/../rssfeedreader.js');
var config        = require(__dirname + '/../config.js');
var boxes         = require(__dirname + '/boxes.js');

var box = [];

var reddit = {
  init: function() {
    box[0] = boxes.createLargeBox('Programmer Jokes');
    box[1] = boxes.createLargeBox('News');

    reddit.doTheReddit1();
    reddit.doTheReddit2();
    setInterval(reddit.tick, 1000 * 60 * config.updateInterval);
  },

  tick: function() {
    reddit.doTheReddit1();
    reddit.doTheReddit2();
  },

  doTheReddit1: function() {
    box[0].content = 'Loading..';
    rssfeedreader.readPageWithSubs('https://www.reddit.com/r/ProgrammingJokes/', function (err, data) {
      if(err) {
        box[0].content = 'There was an error! ' + err;
        return;
      }
      if (box[0].content === 'Loading..') {
        box[0].content = '';
      }
      box[0].content += data;
    });
  },

  doTheReddit2: function() {
    box[1].content = 'Loading..';
    rssfeedreader.readPageWithSubs('https://www.reddit.com/r/UpliftingNews/', function (err, data) {
      if(err) {
        box[1].content = 'There was an error! ' + err;
        return;
      }
      if (box[1].content === 'Loading..') {
        box[1].content = '';
      }
      box[1].content += data;
    });
  }
};

module.exports = reddit;