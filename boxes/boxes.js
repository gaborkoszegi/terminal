/**
 * Created by krisztian.mariassy on 19/05/2017.
 */
var blessed       = require('blessed');
var contrib       = require('blessed-contrib');
var screenModule  = require(__dirname + '/../screen.js');

var maxSize = 24;
var grid;
var matrix = [];

var boxes = {
  init: function() {
    grid = new contrib.grid({rows: maxSize, cols: maxSize, screen: screenModule.getScreen()});
    for(var i = 0; i < maxSize; i++) {
      matrix[i] = [];
      for(var j = 0; j < maxSize; j++) {
        matrix[i][j] = 'e';
      }
    }
  },

  createSmallGrid(title) {
    var w = maxSize / 6;
    var h = maxSize / 3;
    var box = this.createGrid(w, h, title);
    return box;
  },

  createMediumBox(title) {
    var w = maxSize / 4;
    var h = maxSize / 3;
    var box = this.createGrid(w, h, title);
    return box;
  },

  createLargeBox(title) {
    var w = maxSize / 3;
    var h = maxSize / 3;
    var box = this.createGrid(w, h, title);
    return box;
  },

  createGrid(w, h, title) {
    var coords = this.findSpace(w, h);
    var x = coords[0];
    var y = coords[1];

    var box = grid.set(x, y, w, h, blessed.box, this.makeScrollBox(title));
    // Debug
    //box.content = x + ' , ' + y + ' , h: ' + h + ' w: ' + w;
    this.fillMatrix(x, y, w, h);
    return box;
  },

  createGraph(title) {
    var w = 6;
    var h = 6;
    var coords = this.findSpace(w, h);
    var x = coords[1];
    var y = coords[0];

    var box = grid.set(y, x, h, w, blessed.box, this.makeGraphBox(title));
    this.fillMatrix(y, x, h, w);
    return box;
  },

  fillMatrix: function(x, y, maxX, maxY) {
    for (var i = x; i < (maxX + x); i++) {
      for (var j = y; j < (maxY + y); j++) {
        matrix[i][j] = 'f';
      }
    }
  },

  findSpace: function(sizeX, sizeY) {
    var ret = [];
    loop1:
    for(var i = 0; i < (maxSize - sizeX + 1); i++) {
      for(var j = 0; j < (maxSize - sizeY + 1); j++) {
        if(matrix[i][j] === 'e' && matrix[i + sizeX - 1][j] === 'e' && matrix[i][j + sizeY - 1] === 'e'  && matrix[i + sizeX - 1][j + sizeY - 1] === 'e') {
          ret = [i, j];
          break loop1;
        }
      }
    }
    return ret;
  },

  makeBox: function(label) {
    return {
      label: label,
      tags: true,
      // draggable: true,
      border: {
        type: 'line'  // or bg
      },
      style: {
        fg: 'white',
        border: {fg: 'cyan'},
        hover: {border: {fg: 'red'},}
      }
    };
  },

  makeScrollBox: function (label) {
    var options             = this.makeBox(label);
    options.scrollable      = true;
    options.scrollbar       = {ch: '|'};
    options.style.scrollbar = {bg: 'cyan', fg: 'white'};
    options.keys            = true;
    options.vi              = true;
    options.alwaysScroll    = true;
    options.mouse           = true;
    return options;
  },

   makeGraphBox: function(label) {
    var options       = this.makeBox(label);
    options.barWidth  = 5;
    options.xOffset   = 4;
    options.maxHeight = 10;
    return options;
  },

  drawMatrix: function() {
    var row = [];
    for(var i = 0; i < maxSize; i++) {
      for(var j = 0; j < maxSize; j++){
        if(row[i] === undefined) {
          row[i] = matrix[i][j];
        }
        row[i] += matrix[i][j];
      }
    }

    row.forEach(function(val) {
      console.log(val);
    });
  }

};

module.exports = boxes;
