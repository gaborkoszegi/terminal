/**
 * Created by krisztian.mariassy on 18/05/2017.
 */

var config        = require(__dirname + '/../config.js');
var twitterbot    = require(__dirname + '/../twitterbot.js');
var parrotSay     = require('parrotsay-api');
var screenModule  = require(__dirname + '/../screen.js');
var boxes         = require(__dirname + '/boxes.js');

var tweetBoxes = {}, parrotBox;

var twitter = {
  init: function() {
    parrotBox = boxes.createLargeBox(' Parrrroot');
    tweetBoxes[config.twitter[1]] = boxes.createSmallGrid(' 💖 ');
    tweetBoxes[config.twitter[2]] = boxes.createSmallGrid(' 💬 ');

    twitter.tick();
    setInterval(twitter.tick, 1000 * 60 * config.updateInterval);
  },

  tick: function() {
    twitter.doTheTweets();
  },

  doTheTweets: function() {
    for (var which in config.twitter) {
      // Gigantor hack: first twitter account gets spoken by the party parrot.
      if (which == 0) {
        twitterbot.getTweet(config.twitter[which]).then(function (tweet) {
          parrotSay(tweet.text).then(function (text) {
            parrotBox.content = text;
            screenModule.getScreen().render();
          });
        }, function (error) {
          // Just in case we don't have tweets.
          parrotSay('Hi! You\'re doing great!!!').then(function (text) {
            parrotBox.content = text;
            screenModule.getScreen().render();
          });
        });
      } else {
        twitterbot.getTweet(config.twitter[which]).then(function (tweet) {
          tweetBoxes[tweet.bot.toLowerCase()].content = tweet.text;
          screenModule.getScreen().render();
        }, function (error) {
          for (var i = 0, len = tweetBoxes.length; i < len; i++) {
            tweetBoxes[i].content = 'Can\'t read Twitter without some API keys  🐰. Maybe try the scraping version instead?';
          }
        });
      }
    }
  }
};

module.exports = twitter;