/**
 * Created by krisztian.mariassy on 18/05/2017.
 */

var config        = require(__dirname + '/../config.js');
var gitbot        = require(__dirname + '/../gitbot.js');
var misc          = require(__dirname + '/../misc.js');
var spawn         = require('child_process').spawn;
var screenModule  = require(__dirname + '/../screen.js');
var boxes         = require(__dirname + '/boxes.js');

var todayBox, weekBox, commits;

var codes = {

  init: function() {

    todayBox = boxes.createMediumBox(' 📝  Today ');
    weekBox = boxes.createMediumBox(' 📝  Week ');
    //commits = boxes.createGraph('Commits');

    codes.doTheCodes();
    setInterval(codes.tick, 1000 * 60 * config.updateInterval);
  },

  tick: function() {
    codes.doTheCodes();
  },

  doTheCodes: function() {
    var todayCommits = 0;
    var weekCommits  = 0;
    var that = this;

    function getCommits(data, box) {
      var content     = misc.colorizeLog(data || '');
      box.content += content;
      var commitRegex = /(.......) (- .*)/g;
      return (box && box.content) ? (box.content.match(commitRegex) || []).length : '0';
    }

    if (config.gitbot.toLowerCase() === 'gitstandup') {
      var today        = spawn('sh ' + __dirname + '/../standup-helper.sh', ['-m ' + config.depth, config.repos], {shell: true});
      todayBox.content = '';
      today.stdout.on('data', data => {
        todayCommits = getCommits(`${data}`, todayBox);
        //that.updateCommitsGraph(commits, todayCommits, weekCommits);
        screenModule.getScreen().render();
      });

      var week        = spawn('sh ' + __dirname + '/../standup-helper.sh', ['-m ' + config.depth + ' -d 7', config.repos], {shell: true});
      weekBox.content = '';
      week.stdout.on('data', data => {
        weekCommits = getCommits(`${data}`, weekBox);
        //that.updateCommitsGraph(commits, todayCommits, weekCommits);
        screenModule.getScreen().render();
      });
    } else {
      gitbot.findGitRepos(config.repos, config.depth - 1, (err, allRepos) => {
        if (err) {
          return todayBox.content = err;
          screenModule.getScreen().render();
        }
        gitbot.getCommitsFromRepos(allRepos, 1, (err, data) => {
        if (err) {
          return todayBox.content = err;
          screenModule.getScreen().render();
        }
        todayBox.content = '';
      todayCommits       = getCommits(`${data}`, todayBox);
      //that.updateCommitsGraph(commits, todayCommits, weekCommits);
          screenModule.getScreen().render();
    })
      ;
      gitbot.getCommitsFromRepos(allRepos, 7, (err, data) => {
        if (err) {
          return weekBox.content = err;
          screenModule.getScreen().render();
        }
        weekBox.content = '';
      weekCommits       = getCommits(`${data}`, weekBox);
      //that.updateCommitsGraph(commits, todayCommits, weekCommits);
        screenModule.getScreen().render();
    })
      ;
    })
      ;
    }
  }

  /*updateCommitsGraph: function(commits, today, week) {
    commits.setData({titles: ['today', 'week'], data: [today, week]})
  }*/


};

module.exports = codes;