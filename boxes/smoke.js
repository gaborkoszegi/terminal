/**
 * Created by krisztian.mariassy on 24/05/2017.
 */

var chalk         = require('chalk');

var config        = require(__dirname + '/../config.js');
var misc          = require(__dirname + '/../misc.js');
var boxes         = require(__dirname + '/../boxes/boxes.js');
var screenModule  = require(__dirname + '/../screen.js');

var s = 0, m = 0, h = 0;
var box;
var smokeInterval;

var smoke = {

  init: function() {
    var that = this;
    box = boxes.createSmallGrid(' Smoking time counter ');

    box.on('click', function() {
      smoke.resetTime();
    });

    smoke.setText();
    smokeInterval = setInterval(smoke.tick, 1000);
  },

  tick: function() {
    smoke.increaseTime();
    smoke.setText();
  },

  increaseTime: function() {
    s++;
    if(s == 60) {
      s = 0;
      m++;
    }
    if(m == 60) {
      m = 0;
      h++;
    }
  },

  resetTime: function() {
    s = 0; m = 0; h = 0;
    smoke.setText(0,0,0);
  },

  setText: function(a, b, c) {
    a = a || h;
    b = b || m;
    c = c || s;
    box.content = chalk.bold(
      misc.betterTime(a) + ':' +
      misc.betterTime(b) + ':' +
      misc.betterTime(c)) +
      ' since the last smoke.';
    screenModule.getScreen().render();
  }
};

module.exports = smoke;