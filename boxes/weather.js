/**
 * Created by krisztian.mariassy on 18/05/2017.
 */

var weather   = require('weather-js');
var config        = require(__dirname + '/../config.js');
var boxes         = require(__dirname + '/boxes.js');

var box;

var weatherData = {

  init: function() {

    box = boxes.createSmallGrid(' 🌤 ');

    weatherData.doTheWeather();
    setInterval(weatherData.tick, 1000 * 60 * config.updateInterval);
  },

  tick: function() {
    weatherData.doTheWeather();
  },

  doTheWeather: function() {
  weather.find({search: config.weather, degreeType: config.celsius ? 'C' : 'F'}, function (err, result) {
    if (result && result[0] && result[0].current) {
      var json           = result[0];
      // TODO: add emoji for this thing.
      var skytext        = json.current.skytext.toLowerCase();
      var currentDay     = json.current.day;
      var degreetype     = json.location.degreetype;
      var forecastString = '';
      for (var i = 0; i < json.forecast.length; i++) {
        var forecast = json.forecast[i];
        if (forecast.day === currentDay) {
          var skytextforecast = forecast.skytextday.toLowerCase();
          forecastString = 'Today, it will be ' + chalk.bold(skytextforecast) + ' with a forecast high of ' + chalk.bold(forecast.high + '°C') + ' and a low of ' + chalk.bold(forecast.low + '°C');
        }
      }
      box.content = 'In ' + json.location.name + ' it\'s ' + chalk.bold(json.current.temperature + '°C') + ' and ' + chalk.bold(skytext) + ' right now.' + forecastString;
    } else {
      box.content = 'Having trouble fetching the weather for you :(';
    }
  });
}
};

module.exports = weatherData;