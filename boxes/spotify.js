/**
 * Created by krisztian.mariassy on 24/05/2017.
 */

var chalk         = require('chalk');
var applescript   = require('applescript');
var fs            = require('fs');

var config        = require(__dirname + '/../config.js');
var misc          = require(__dirname + '/../misc.js');
var boxes         = require(__dirname + '/../boxes/boxes.js');
//var spawn         = require('child_process').spawn;
var screenModule  = require(__dirname + '/../screen.js');

var box;

var spotify = {

  init: function() {
    box = boxes.createSmallGrid('🎵  Spotify 🎵' );

    box.on('click', function() {
      spotify.skipSong();
    });

    spotify.tick();
    setInterval(spotify.tick, 1000 * 5);
  },

  tick: function() {
    spotify.doTheMusic();
  },

  skipSong: function() {
    var skipSongCommand = 'tell application "Spotify" to next track';

    applescript.execString(skipSongCommand, function(err, rtn) {
      if (err) {
        fs.writeFile("log.str", err);
      }
      spotify.doTheMusic();
    });
  },

  doTheMusic: function() {
    var currentSongCommand = 'tell application "Spotify" \n' +
      'set song to current track \n' +
      'set info to "Song: " & name of song \n' +
      'set info to info & " \n'+
      '" & "Album: " & album of song \n'+
      'set info to info & " \n'+
      '" & "Artist: " & artist of song \n'+
      'end tell';

    applescript.execString(currentSongCommand, function(err, rtn) {
      if (err) {
        fs.writeFile("log.str", err);
      }
      box.content = rtn;
      screenModule.getScreen().render();
    });
  }
};

module.exports = spotify;