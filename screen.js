/**
 * Created by krisztian.mariassy on 22/05/2017.
 */

var blessed   = require('blessed');

var screen;

var screenModule = {

  init: function() {
    return this.setScreen();
  },

  setScreen: function() {
    screen = blessed.screen({
      fullUnicode: true, // emoji or bust
      smartCSR: true,
      autoPadding: true,
      title: 'Terminus plus minus'
    });
    return screen;
  },

  getScreen: function() {
    if(screen === undefined) {
      this.setScreen();
    }
    return screen;
  }
};

module.exports = screenModule;