/**
 * Created by krisztian.mariassy on 18/05/2017.
 */

var main          = require(__dirname + '/main.js');
var screenModule  = require(__dirname + '/screen.js');

var keyboard = {

  init: function() {

    screenModule.getScreen().key(['escape', 'q', 'C-c'], function(ch, key) {
      //TODO
      return main.process.exit(0);
    });

    screenModule.getScreen().key(['r', 'C-r'], function(ch, key) {
      main.tick();
    });

  }
};

module.exports = keyboard;