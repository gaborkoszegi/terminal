#!/usr/bin/env node
var config        = require(__dirname + '/config.js');
var keyboard      = require(__dirname + '/keyboard.js');
var boxes         = require(__dirname + '/boxes/boxes.js');
var weather       = require(__dirname + '/boxes/weather.js');
var twitter       = require(__dirname + '/boxes/tweets.js');
var codes         = require(__dirname + '/boxes/codes.js');
var reddit        = require(__dirname + '/boxes/reddit.js');
var smoke         = require(__dirname + '/boxes/smoke.js');
var spotify         = require(__dirname + '/boxes/spotify.js');
var spawn         = require('child_process').spawn;

var main = (function() {

  return {

    init: function () {
      boxes.init();
      keyboard.init();

      //init the boxes
      reddit.init();
      codes.init();
      twitter.init();
      weather.init();
      smoke.init();
      spotify.init();

      //DEBUG
      boxes.drawMatrix();
    },
  };
})();

main.init();

module.exports = main;