/**
 * Created by krisztian.mariassy on 18/05/2017.
 */
var chalk     = require('chalk');

var misc = {

  colorizeLog: function(text) {
    var lines        = text.split('\n');
    var regex        = /(.......) (- .*) (\(.*\)) (<.*>)/i;
    var nothingRegex = /Seems like .* did nothing/i;
    for (var i = 0; i < lines.length; i++) {
      // If it's a path
      if (lines[i][0] === '/') {
        lines[i] = formatRepoName(lines[i], '/')
      } else if (lines[i][0] === '\\') {
        lines[i] = formatRepoName(lines[i], '\\')
      } else {
        // It may be a mean "seems like .. did nothing!" message. Skip it
        var nothing = lines[i].match(nothingRegex);
        if (nothing) {
          lines[i] = '';
          continue;
        }

        // It's a commit.
        var matches = lines[i].match(regex);
        if (matches) {
          lines[i] = chalk.red(matches[1]) + ' ' + matches[2] + ' ' +
            chalk.green(matches[3])
        }
      }
    }
    return lines.join('\n');
  },

  formatRepoName: function(line, divider) {
    var path = line.split(divider);
    return '\n' + chalk.yellow(path[path.length - 1]);
  },

  betterTime: function(time) {
    if(time < 10) {
      time = '0' + time;
    }
    return time;
  }
};

module.exports = misc;