var parser      = require('rss-parser');
var striptags   = require('striptags');
const Entities  = require('html-entities').AllHtmlEntities;
var chalk       = require('chalk');

var rssfeedreader = {
  readFeed: function(url, callback) {
    parser.parseURL(url + '.rss', function(err, parsed) {
        if(err) {
          callback(err, undefined);
          return;
        }
        callback(err, parsed.feed);
    });
  },

  readPageWithSubs: function(url, callback) {
    var that = this;
    that.readFeed(url, function(err, feed) {
      if(err) {
        callback(err, undefined);
        return;
      }
      feed.entries.forEach(function(entry) {
        that.readPage(entry.link, function(err, data) {
          callback(err, data);
        })
      });
    });
  },

  readPage: function(url, callback) {
    var that = this;
    that.readFeed(url, function(err, feed) {
      if(err) {
        callback(err, undefined);
        return;
      }
      var prettyFeed = that.prettyContent(feed.entries[0].content);
      if(prettyFeed.charAt(0) == ' ') {
        return;
      }
      var ret = chalk.green(chalk.bold(feed.entries[0].title)) + '\n' + prettyFeed + '\n\n';
      callback(err, ret);
    });
  },

  prettyContent: function(content) {
    var entities = new Entities();
    var text = striptags(content, [], '');
    var decoded = entities.decode(text);
    var ret = this.removeJunk(decoded);
    return ret;
  },

  removeJunk: function(data) {
    data = data.replace('[link]', '');
    data = data.replace('[comments]', '');
    data = data.replace('submitted by', '');
    if(data.indexOf('/u/') !== -1) {
      data = data.substring(0, data.indexOf('/u/'));
    }
    return data;
  }
};

module.exports = rssfeedreader;
